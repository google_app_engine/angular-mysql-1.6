<?php

class API
{
	
	const SERVER = "localhost";
	const USER = "root";
	const PASSWORD = "";
	const DB = "user-interface";

	function __construct()
	{
		$this->connection();	 
	}

	private function connection(){
		header("Access-Control-Allow-Origin: *");
		header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
		header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");

		$this->connection = mysqli_connect(self::SERVER, self::USER, self::PASSWORD, self::DB);
	}

	private function runQuery($query){
		return mysqli_query($this->connection, $query);
	}

	public function process(){
		if(!isset($_REQUEST['x'])){
			var_dump("REQUEST - NOT DEFINED"); die();
		}
		$func = strtolower(trim(str_replace("/","",$_REQUEST['x'])));

		if((int)method_exists($this,$func) > 0)
			$this->$func();
		else{
			die("404 -  invalid request");
		}

	}	  

	public function getUsers(){

		$this->isMethodAllowed("POST");

		$query = "SELECT * from users order by id ASC";
		$results = $this->runQuery($query);
		
		if(mysqli_num_rows($results) != 0) {
			echo $json_info = json_encode($this->prepareData($results));
		}
	}

	public function insertUser(){

		$this->isMethodAllowed("POST");

		$data = json_decode(file_get_contents("php://input"));

		$first_name =  $data->first_name ;
		$last_name  =  $data->last_name ;
		$email      =  $data->email;
		$born_date  =  date("Y-m-d", strtotime($data->born_date) );
		$address    =  $data->address ;

		$query = "INSERT into users (first_name,last_name,email,born_date,address ) VALUES ('$first_name','$last_name','$email','$born_date','$address')";

		$this->runQuery($query);
		echo true;
	}

	public function updateUser(){

		$this->isMethodAllowed("POST");

		$data = json_decode(file_get_contents("php://input"));

		$id         =  $data->id;
		$first_name =  $data->first_name;
		$last_name  =  $data->last_name ;
		$email      =  $data->email;
		$born_date  =  date("Y-m-d", strtotime($data->born_date) );
		$address    =  $data->address;

		$query = "UPDATE users SET first_name='$first_name',last_name='$last_name',email='$email',born_date='$born_date', address='$address' WHERE id = $id ";

		$this->runQuery($query);
		echo true;
	}


	public function deleteUser(){
		
		$data = json_decode(file_get_contents("php://input"));

		$query = "DELETE FROM users WHERE id ='$data->id'";

		$this->runQuery($query);
		echo true;
	}


	public function cronJob(){

		$query = "INSERT into users (first_name,last_name,email,born_date,address ) VALUES ('CRON','CRON','cron@hotmail.com','".date("Y-m-d")."','LOL')";

		$this->runQuery($query);

	}

	private function isMethodAllowed($type){
		if ($_SERVER['REQUEST_METHOD'] !== $type) {
			die("NOT ".$type." CALL");
		}
		return true;  
	}

	private function prepareData($data){
		$array = array();
		while($row = mysqli_fetch_assoc($data)) {
			$array[] = $row;
		}
		return $array;
	}

}
$p = new API();
$p->process();
