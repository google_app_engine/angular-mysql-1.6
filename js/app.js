

//to mysql api connection.
var _api_route = 'http://localhost/angular-mysql-1.6/api/api.php?x=';

var crudApp = angular.module('MyApp',[]);

 
crudApp.controller('MyController', ['$scope','$http', function($scope, $http) {
	 
 	    $scope.user = {};
 	    $scope.user.action = 'create';


		function loadPoints(){
			var users = firebase.database().ref().child('/contagens');
			users.on('value', snapshot => {
				$scope.point = snapshot.val().points;
				$(".points").text(snapshot.val().points);
			 
			});

			console.log($scope.point);
		}

		
		 
    	$scope.points = function(point){
			$scope.point = point + 1;	
    		firebase.database().ref('contagens').set({
			   points: $scope.point,

			});

			loadPoints();
      	}
       


 		function getUsers(){
	 		// get all users from database
			$http.post(_api_route+'getUsers').then(function(results) {
				$scope.details = results.data;
			});

 		}
 		getUsers();
		

		$scope.create = function(user, action) {
			var target = action == 'create' ? 'insertUser' : 'updateUser';
				$http.post(_api_route+target, user).then(function(result) {
					if(result){
						 
						getUsers();
						setTimeout(function(){ loadPoints(); }, 3000);
					}
				});			
      	}

		$scope.edit = function(user) {
			$scope.user = user;
			$scope.user.action = 'edit';
      	};

      	$scope.delete = function(user){
      		console.log(user);
			$http.post(_api_route+'deleteUser', user).then(function(result) {
				if(result){
					 
					getUsers();
					setTimeout(function(){ loadPoints(); }, 3000);
					 
				}
			});		      		
      	}
		loadPoints(); 


}]);